import cv2
import face_recognition as fr


KNOWN_FACES_DIR = "Face/KnownFaces"

def main():
    cap = cv2.VideoCapture(0)
    i = 0
    while True:
        _, frame = cap.read()

        try:

            faceLoc = fr.face_locations(frame)[0]
            cv2.imwrite(f"{KNOWN_FACES_DIR}/{i}.jpg", frame)
            print('Save complete')
            i+=1
            print(faceLoc)
            y1, x2, y2, x1 = faceLoc
            cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 255), 2)

        except:
            pass
        cv2.imshow('camera', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            break


if __name__ == '__main__':
    main()