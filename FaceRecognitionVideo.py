import cv2
import face_recognition as fr
import os
id = ['1','2']

KNOWN_FACES_DIR = "Face/KnownFaces"
TOLERANCE = 0.6
FRAME_THICKNESS = 3
FONT_THICKNESS = 2
MODEL = "hog"


def loadKnownFaces(path):

    known_faces = []
    known_names = []
    print('Load Known Faces')
    for filename in os.listdir(path):
        image = fr.load_image_file(f"{path}/{filename}")
        try:
            encoding = fr.face_encodings(image)[0]
        except:
            pass
        known_faces.append(encoding)
        known_names.append(id[0])
    return known_faces, known_names


def main():

    known_faces, known_names = loadKnownFaces(KNOWN_FACES_DIR)
    cap = cv2.VideoCapture(0)
    print('Math Find')
    while True:
        _, frame = cap.read()
        locations = fr.face_locations(frame, model=MODEL)
        encodings = fr.face_encodings(frame, locations)

        #'Compare Faces'
        for face_encoding, face_location in zip(encodings, locations):
            results = fr.compare_faces(known_faces, face_encoding, TOLERANCE)
            match = None

            #'Results'
            if True in results:
                match = known_names[results.index(True)]
                print(f"Match found: {match}")
                y1, x2, y2, x1 = face_location
                color = [0, 255, 0]
                cv2.rectangle(frame, (x1, y1), (x2, y2), color, FRAME_THICKNESS)
                cv2.rectangle(frame, (x1, y2), (x2, y2+22), color, cv2.FILLED)
                cv2.putText(frame, match, (x1+10, y2+10), cv2.FONT_HERSHEY_SIMPLEX,
                            0.5, (200, 200, 200), FONT_THICKNESS)

        cv2.imshow('face', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
            break


if __name__ == '__main__':
    main()