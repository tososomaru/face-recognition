import cv2
import face_recognition as fr
import os

KNOWN_FACES_DIR = "Face/KnownFaces"
UNKNOWN_FACES_DIR = "Face/UnknownFaces"
TOLERANCE = 0.6
FRAME_THICKNESS = 3
FONT_THICKNESS = 2
MODEL = "hog"
NAME = 'Nikita'

def main():
    known_faces = []
    known_names = []

    print('Load Known Faces')
    for filename in os.listdir(KNOWN_FACES_DIR):
        image = fr.load_image_file(f"{KNOWN_FACES_DIR}/{filename}")
        try:
            encoding = fr.face_encodings(image)[0]
        except:
            pass
        known_faces.append(encoding)
        known_names.append(NAME)

    print('Load Unknown Faces')
    for filename in os.listdir(UNKNOWN_FACES_DIR):
        image = fr.load_image_file(f"{UNKNOWN_FACES_DIR}/{filename}")
        locations = fr.face_locations(image, model=MODEL)
        encodings = fr.face_encodings(image, locations)
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

        #'Compare Faces'
        for face_encoding, face_location in zip(encodings, locations):
            results = fr.compare_faces(known_faces, face_encoding, TOLERANCE)
            match = None

            #'Results'
            if True in results:
                match = known_names[results.index(True)]
                print(f"Match found: {match}")
                y1, x2, y2, x1 = face_location
                color = [0, 255, 0]
                cv2.rectangle(image, (x1, y1), (x2, y2), color, FRAME_THICKNESS)
                cv2.rectangle(image, (x1, y2), (x2, y2+22), color, cv2.FILLED)
                cv2.putText(image, match, (x1+10, y2+10), cv2.FONT_HERSHEY_SIMPLEX,
                            0.5, (200, 200, 200), FONT_THICKNESS)

        cv2.imshow(filename, image)
        cv2.waitKey(100000)


if __name__ == '__main__':
    main()